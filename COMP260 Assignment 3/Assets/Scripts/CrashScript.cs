﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CrashScript : MonoBehaviour {
	public Text CrashText;
	public Text ScoreText;
	public Text EndText;
	public int ScorePerCoin = 1;
	int score = 0;
	public AudioClip coingrab;
	public AudioClip crash;
	private AudioSource audio2;
	private AudioSource audio;
	bool crashplay;

	void Start () {
		ScoreText.text = "Score:" + score;
		audio = GetComponent<AudioSource> ();
		audio2 = GetComponent<AudioSource> ();
		crashplay = false;
	}
	
	void Update () {
		if (Time.timeScale == 0) {
			if (crashplay == false) {
				audio2.PlayOneShot (crash);
				crashplay = true;
			}
			CrashText.text = "You Crashed, press SPACE to replay!";
			if (Input.GetKeyDown ("space")) {
				Application.LoadLevel ("Scene1");
				Time.timeScale = 1f;
				CrashText.text = "";
				score = 0;
			}
		}


			
	}

	public void EndGame () {
		EndText.text = "Level Complete, Press SPACE to play again";
	}

	public void CountScore () {
		score += ScorePerCoin;
		ScoreText.text = "Score:" + score;
		audio.PlayOneShot(coingrab);

	}


}
