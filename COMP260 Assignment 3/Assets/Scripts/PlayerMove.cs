﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	public float speed = 5.0f;
	public Transform Startpos;
	public AudioClip bump;
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 direction;
		direction.x = Input.GetAxis ("Horizontal");
		direction.y = Input.GetAxis ("Vertical");

		Vector2 velocity = direction * speed;
		transform.Translate (velocity * Time.deltaTime);
	}

	void OnTriggerEnter (Collider col) {
		if (col.tag == "Finish") {
			CrashScript endgame = FindObjectOfType<CrashScript> ();
			endgame.EndGame ();
		}
			
			
	
}
	void OnCollisionEnter (Collision col) {
		audio.PlayOneShot (bump);
	}
			
}
