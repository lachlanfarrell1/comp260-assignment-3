﻿using UnityEngine;
using System.Collections;

public class CoinPickup : MonoBehaviour {
	

	void Start () {
	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			CrashScript score = FindObjectOfType<CrashScript> ();
			score.CountScore ();
			Destroy (gameObject);
		}
	}



}
